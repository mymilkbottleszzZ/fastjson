package me.mymilkbottles.fastjson.basic;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

public class FastJSONTest {

    @Test
    public void testDebugFastJSON() {
        User user = new User("hello_name", "hello_passwd", 18);
        String userToString = JSON.toJSONString(user);
        System.out.println(userToString);
    }

}
