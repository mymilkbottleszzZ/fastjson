package me.mymilkbottles.fastjson.basic;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class CycleRefTest {

    /**
     * 当进行toJSONString的时候，默认如果重用对象的话，会使用引用的方式进行引用对象。
     */
    @Test
    public void testCycleRef() {
        Map<String, User> maps = new HashMap<String, User>();
        User s1 = new User("s", "s", 16);

        maps.put("s1", s1);
        maps.put("s2", s1);

        System.out.println(JSON.toJSONString(maps));
        // {"s1":{"age":16,"passWord":"s","userName":"s"},"s2":{"$ref":"$.s1"}}

        System.out.println(JSON.toJSONString(maps, SerializerFeature.DisableCircularReferenceDetect));
        // {"s1":{"age":16,"passWord":"s","userName":"s"},"s2":{"age":16,"passWord":"s","userName":"s"}}
    }

}
